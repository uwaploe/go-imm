package imm

import (
	"bytes"
	"encoding/xml"
	"testing"
	"time"
)

type rwbuffer struct {
	rbuf *bytes.Buffer
	wbuf *bytes.Buffer
}

func (b *rwbuffer) Read(p []byte) (int, error) {
	return b.rbuf.Read(p)
}

func (b *rwbuffer) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

type rwbuffer_timeo struct {
	rbuf  *bytes.Buffer
	wbuf  *bytes.Buffer
	timeo time.Duration
	count int
}

func (b *rwbuffer_timeo) Read(p []byte) (int, error) {
	b.count++
	if b.count > 1 {
		time.Sleep(b.timeo)
	}
	return b.rbuf.Read(p)
}

func (b *rwbuffer_timeo) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestWakeup(t *testing.T) {
	resp := []byte("\r\nIMM>")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}

	imm := NewIMM(&rw)
	if imm.Mode() != ModeIMService {
		t.Fatalf("Bad IMM Mode: %v", imm.Mode())
	}
	err := imm.Wakeup()
	if err != nil {
		t.Fatal(err)
	}
	if imm.Mode() != ModeHost {
		t.Errorf("Bad IMM Mode: %v", imm.Mode())
	}
}

func TestSleep(t *testing.T) {
	resp := []byte("\r\n<Executed/>\r\n<PowerOff/>\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.mode = ModeHost

	err := imm.Sleep()
	if err != nil {
		t.Fatal(err)
	}
	if imm.Mode() != ModeIMService {
		t.Errorf("Bad IMM Mode: %v", imm.Mode())
	}
}

func TestIMMError(t *testing.T) {
	resp := []byte("<Error type='INVALID' msg='Illegal command'/>\r\n<Executed/>\r\nIMM>")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.SetMode(ModeHost)

	_, err := imm.Recv()
	_, ok := err.(*IMMError)
	if !ok {
		t.Errorf("Unexpected error type: %v", err)
	}
}

func TestIMMErrorAlt(t *testing.T) {
	resp := []byte("<ERROR type='INVALID' msg='Illegal command'/>\r\n<Executed/>\r\nIMM>")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.SetMode(ModeHost)

	_, err := imm.Recv()
	_, ok := err.(*IMMErrorAlt)
	if !ok {
		t.Errorf("Unexpected error type: %v", err)
	}
}

func TestIMMode(t *testing.T) {
	resp := []byte("Line 1\r\nLine 2\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.SetMode(ModeIMService)

	reply, err := imm.Recv()
	if err != nil {
		t.Fatal(err)
	}
	if reply != "Line 1" {
		t.Fatalf("Unexpected reply: %q", reply)
	}
	reply, err = imm.Recv()
	if reply != "Line 2" {
		t.Fatalf("Unexpected reply: %q", reply)
	}
}

func TestRemote(t *testing.T) {
	resp := []byte("<RemoteReply>Hello</RemoteReply>\r\n<Executed/>\r\nIMM>")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.SetMode(ModeHost)

	reply, err := imm.Recv()
	if err != nil {
		t.Fatalf("Error: %v", err)
	}
	if reply != "Hello" {
		t.Fatalf("Unexpected reply: %q", reply)
	}
}

func TestDiscovery(t *testing.T) {
	resp := []byte("<TimeoutICD>8000</TimeoutICD>\r\n<Discovery>\r\n<Discovered SN='70002445'/>\r\n</Discovery>\r\n<Executed/>\r\nIMM>")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.SetMode(ModeHost)

	reply, err := imm.RecvXml()
	if err != nil {
		t.Fatalf("Error: %v", err)
	}

	if len(reply.Peers) != 1 {
		t.Fatalf("Response not parsed: %v", reply)
	}

	if reply.Peers[0].Peer_sn != "70002445" {
		t.Fatalf("Bad s/n: %v", reply.Peers[0].Peer_sn)
	}
}

func TestEmptyDiscovery(t *testing.T) {
	resp := []byte("<TimeoutICD>8000</TimeoutICD>\r\n<Discovery>\r\n</Discovery>\r\n<Executed/>\r\nIMM>")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.SetMode(ModeHost)

	reply, err := imm.RecvXml()
	if err != nil {
		t.Fatalf("Error: %v", err)
	}

	if len(reply.Peers) != 0 {
		t.Fatalf("Response not parsed: %v", reply)
	}
}

func TestBadXml(t *testing.T) {
	resp := []byte("<RemoteReply>Hel<<o</RemoteReply>\r\n<Executed/>\r\nIMM>")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	imm := NewIMM(&rw)
	imm.SetMode(ModeHost)

	_, err := imm.Recv()
	_, ok := err.(*xml.SyntaxError)
	if !ok {
		t.Errorf("Unexpected error type: %v", err)
	}
}
