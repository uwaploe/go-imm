// Package imm provides an interface to a Seabird Inductive Modem
// Module.
package imm

import (
	"bytes"
	"context"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"strings"
	"sync"
	"time"
)

type IMMMode int

const (
	// Accepts commands from the local host
	ModeHost IMMMode = iota
	// Accepts output from the remote host
	ModeIMService IMMMode = iota
)

const prompt = "IMM>"
const power_down = "<PowerOff/>"

// XML encoded error tag from the device
type IMMError struct {
	XMLName xml.Name `xml:"Error"`
	Type    string   `xml:"type,attr"`
	Msg     string   `xml:"msg,attr"`
}

type IMMErrorAlt struct {
	XMLName xml.Name `xml:"ERROR"`
	Type    string   `xml:"type,attr"`
	Msg     string   `xml:"msg,attr"`
}

func (e *IMMError) Error() string {
	return e.Msg
}

func (e *IMMErrorAlt) Error() string {
	return e.Msg
}

type IMMTimeout struct {
	XMLName xml.Name `xml:"TIMEOUT"`
	Msg     string   `xml:"msg,attr"`
}

func (e *IMMTimeout) Error() string {
	return e.Msg
}

// Reply from the remote host
type RemoteReply struct {
	Text string `xml:",chardata"`
}

// Return value from the `disc` command
type Discovered struct {
	Peer_sn string `xml:"SN,attr"`
}

// Structure to hold the parsed XML response from the
// device. This does not handle all of the possible
// responses but, for now, all we care about are Error's
// RemoteReply's, and Discovered ...
type Reply struct {
	XMLName     xml.Name `xml:"Reply"`
	Error       IMMError
	ErrorAlt    IMMErrorAlt
	Timeout     IMMTimeout
	RemoteReply RemoteReply
	Peers       []Discovered `xml:"Discovery>Discovered"`
}

// Interface to an Inductive Modem
type IMM struct {
	rdr   io.Reader
	wtr   io.Writer
	mode  IMMMode
	debug bool
	wg    sync.WaitGroup
	Read  func() (string, error)
}

func readLine(rdr io.Reader) (string, error) {
	c := make([]byte, 1)
	buf := make([]byte, 0)
	n, err := rdr.Read(c)
	for n > 0 {
		buf = append(buf, c[0])
		if err != nil || c[0] == '\n' {
			break
		}
		n, err = rdr.Read(c)
	}

	if err != nil {
		return "", err
	}

	return strings.TrimRight(string(buf), "\r\n"), nil
}

func readResponse(rdr io.Reader, match []byte) (string, error) {
	c := make([]byte, 1)
	buf := make([]byte, 0)
	n, err := rdr.Read(c)
	for n > 0 {
		buf = append(buf, c[0])
		if err != nil || bytes.HasSuffix(buf, match) {
			break
		}
		n, err = rdr.Read(c)
	}

	if err != nil {
		return "", err
	}

	return string(buf), nil
}

// NewIMM returns a new IMM attached to the specified i/o interface.
func NewIMM(port io.ReadWriter) *IMM {
	imm := IMM{}
	imm.rdr = port
	imm.wtr = port
	imm.SetMode(ModeIMService)

	return &imm
}

func (imm *IMM) SetTimeout(_ time.Duration) {
}

// Enable or disable debugging output.
func (imm *IMM) SetDebug(state bool) {
	imm.debug = state
}

// Wakeup puts the IMM into Host mode. In Host mode, the device will
// accept commands from the host computer.
func (imm *IMM) Wakeup() error {
	if imm.mode == ModeHost {
		return nil
	}

	imm.SetMode(ModeHost)
	if imm.debug {
		log.Printf("SEND: %q\n", "\r")
	}

	imm.wtr.Write([]byte("\r"))
	_, err := imm.Read()

	if err != nil {
		imm.wtr.Write([]byte("\n"))
		_, err = imm.Read()
		if err != nil {
			imm.SetMode(ModeIMService)
			log.Println("Cannot wake IMM")
			return err
		}
	}

	return nil
}

// Sleep puts the IMM into IMM mode. In this mode, the device passes all
// data recieved over the inductive link.
func (imm *IMM) Sleep() error {
	if imm.mode == ModeIMService {
		return nil
	}

	imm.Read = func() (string, error) {
		s, err := readResponse(imm.rdr, []byte(power_down))
		if imm.debug {
			log.Printf("RECV: %q\n", string(s))
		}
		return s, err
	}

	imm.wtr.Write([]byte("pwroff\r\n"))
	_, err := imm.Read()
	if err != nil {
		log.Println("Cannot power down IMM")
		return err
	}
	imm.SetMode(ModeIMService)

	return nil
}

// Mode returns the mode of the device.
func (imm *IMM) Mode() IMMMode {
	return imm.mode
}

// SetMode sets the mode of the device.
func (imm *IMM) SetMode(mode IMMMode) {
	switch mode {
	case ModeHost:
		imm.mode = mode
		imm.Read = func() (string, error) {
			s, err := readResponse(imm.rdr, []byte(prompt))
			if imm.debug {
				log.Printf("RECV: %q\n", string(s))
			}
			return s, err
		}
	case ModeIMService:
		imm.mode = mode
		imm.Read = func() (string, error) {
			s, err := readLine(imm.rdr)
			if imm.debug {
				log.Printf("RECV: %q\n", string(s))
			}
			return s, err
		}
	}
}

// Send sends a string to the device.
func (imm *IMM) Send(cmd string) error {
	if imm.debug {
		log.Printf("SEND: %q\n", cmd)
	}

	_, err := imm.wtr.Write([]byte(cmd))
	if err == nil && !strings.HasSuffix(cmd, "\r\n") {
		imm.wtr.Write([]byte("\r\n"))
	}
	return err
}

// Recv returns the response string from the device along with any
// error that occurred.
func (imm *IMM) Recv() (string, error) {
	resp, err := imm.Read()

	if err == nil && imm.mode == ModeHost {
		// Parse the XML reply to check for errors. We need to wrap
		// the raw reply in a <Reply> tag because it is not strict
		// XML.
		buf := []byte("<Reply>")
		v := Reply{}
		buf = append(append(buf, resp...), []byte("</Reply>")...)
		err = xml.Unmarshal(buf, &v)

		if err == nil {
			// Check if the IMM sent an Error response
			if v.Error.Type != "" {
				err = &v.Error
			} else if v.ErrorAlt.Type != "" {
				err = &v.ErrorAlt
			}

			if v.Timeout.Msg != "" {
				err = &v.Timeout
			}

			// Check for a RemoteReply, if present, send that as the
			// response since that is what we really care about.
			if v.RemoteReply.Text != "" {
				resp = v.RemoteReply.Text
			}
		}
	}
	return resp, err
}

// Recv returns a structure containing the parsed XML reply along with
// any error that occurred.
func (imm *IMM) RecvXml() (*Reply, error) {
	resp, err := imm.Read()

	v := Reply{}
	if err == nil && imm.mode == ModeHost {
		// Parse the XML reply to check for errors. We need to wrap
		// the raw reply in a <Reply> tag because it is not strict
		// XML.
		buf := []byte("<Reply>")
		buf = append(append(buf, resp...), []byte("</Reply>")...)
		err = xml.Unmarshal(buf, &v)
	}

	return &v, err
}

// Check for peers
func (imm *IMM) FindPeers() ([]string, error) {
	var err error
	var peers []string

	if imm.mode != ModeHost {
		return peers, fmt.Errorf("not in Host mode")
	}

	err = imm.CaptureLine(true)
	if err != nil {
		return peers, err
	}

	err = imm.Send("disc")
	resp, err := imm.RecvXml()
	if err == nil {
		if len(resp.Peers) > 0 {
			peers = make([]string, len(resp.Peers))
			for i, p := range resp.Peers {
				peers[i] = p.Peer_sn
			}
		}
	}

	return peers, err
}

// Update an IMM configuration setting
func (imm *IMM) Set(key string, value interface{}) error {
	var err error

	if imm.mode != ModeHost {
		return fmt.Errorf("not in Host mode")
	}
	err = imm.Send(fmt.Sprintf("set%s=%v", key, value))
	if err != nil {
		return err
	}
	_, err = imm.Recv()
	return err
}

// Capture the IM line. Return any error that occurs.
func (imm *IMM) CaptureLine(force bool) error {
	var err error
	if force {
		err = imm.Send("fcl")
	} else {
		err = imm.Send("captureline")
	}
	if err != nil {
		return err
	}
	_, err = imm.Recv()
	return err
}

// Release the IM line. Return any error that occurs.
func (imm *IMM) ReleaseLine() error {
	var err error

	if imm.mode != ModeHost {
		return fmt.Errorf("not in Host mode")
	}
	err = imm.Send("releaseline")
	if err != nil {
		return err
	}
	_, err = imm.Recv()
	return err
}

// Send a message to a Group. Return any error that occurs
func (imm *IMM) GroupMessage(id int, msg string) error {
	var err error

	if imm.mode != ModeHost {
		return fmt.Errorf("not in Host mode")
	}
	err = imm.Send(fmt.Sprintf("#G%d:%s", id, msg))
	if err != nil {
		return err
	}
	_, err = imm.Recv()
	return err
}

// Send a message to a Host. Return the reply and any error that occurs
func (imm *IMM) HostMessage(id int, msg string) (string, error) {
	var err error

	if imm.mode != ModeHost {
		return "", fmt.Errorf("not in Host mode")
	}
	err = imm.Send(fmt.Sprintf("#S%d:%s", id, msg))
	if err != nil {
		return "", err
	}
	return imm.Recv()
}

// Return two channels; one for responses and one for any error
// that occurs. Timeout errors can be ignored by setting the
// ignore_timeo argument to true. The response channel will
// produce output until the Context is cancelled.
func (imm *IMM) Reader(ctx context.Context,
	ignore_timeo bool) (<-chan string, <-chan error) {
	out := make(chan string, 1)
	errc := make(chan error, 1)
	imm.wg.Add(1)

	go func() {
		defer close(out)
		defer imm.wg.Done()
		for {
			msg, err := imm.Recv()
			if !ignore_timeo && err == io.EOF {
				errc <- err
				return
			}
			select {
			case out <- msg:
			case <-ctx.Done():
				errc <- ctx.Err()
			}
		}
	}()
	return out, errc
}

// Wait for the Reader to finish
func (imm *IMM) ReaderWait() {
	imm.wg.Wait()
}
